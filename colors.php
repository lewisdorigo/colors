<?php
/**
 * Plugin Name:       Colors
 * Plugin URI:        https://bitbucket.org/lewisdorigo/colors
 * Description:       A simple WordPress plugin to extract colors array from a constants file, and work with them in a sensible way.
 * Version:           1.0.2
 * Author:            Lewis Dorigo
 * Author URI:        https://dorigo.co/
 */


if ( !defined( 'ABSPATH' ) ) {
	die();
}

require_once __DIR__.'/lib/Colors.php';