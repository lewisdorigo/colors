<?php namespace Dorigo\Colors;

use \Dorigo\Singleton\Singleton;
use \Dorigo\Constants\Constants;

class Colors extends Singleton {
    private $colors = [];
    private $list = [];

    protected function __construct() {
        $this->colors = Constants::getInstance()->get('colors', []);

        $list = [];

        foreach($this->colors as $slug => $color) {
            $name = str_replace(['-','_'], ' ', $slug);
            $list[$slug] = ucwords($name);
        }

        $this->list = $list;

        if(function_exists("add_filter")) {
            add_filter('Dorigo/Site/Colors', [$this, 'getList'], 1, 0);
        }
    }

    private function isNamedColor($name, $list = null) {
        $list = is_null($list) ? $this->list : $list;

        if(is_object($name) || is_array($name)) {
            $name = current($name);
        };

        return is_string($name) && isset($list->{$name});
    }

    private function __color($name, $list, ...$types) {
        if(!$this->isNamedColor($name, $list)) {
            throw new \OutOfBoundsException("The color `${$name}` does not exist in requested list.");
            die;
        }

        $data = $list->{$name};

        if($this->isNamedColor($data)) {
            $data = is_string($data) ? [$data] : $data;
            return $this->getColor(...$data);

        } elseif($this->isNamedColor($data, $list)) {

            $name = current($data);
            $data = array_shift($data);

            return $this->__color($name, $list, ...$data);

        } elseif(is_object($data) || is_array($name)) {
            if(count($types) < 1) {
                $types = ["color"];
            }

            $type = $types[0];
            $rest = array_shift($types);
            $rest = is_string($rest) ? [$rest] : $rest;

            if(isset($data->{$type})) {

                if($this->isNamedColor($type, $data)) {
                    return $this->__color($type, $data, ...$rest);
                } else {
                    return $data;
                }

            } else if($this->isNamedColor("color", $data)) {

                $color = $this->__color("color", $data);

            } else {

                throw new \OutOfBoundsException("Could not find color `${$name}(".implode(", ", $types).")`.");
                die;

            }
        } else {
            return $data;
        }


        throw new \Exception("Something went wrong trying to find the color `${$name}`.");
        die;
    }

    public function getColor($name, ...$types) {
        return $this->__color($name, $this->colors, ...$types);
    }

    public function isHexColor($color) {
        return preg_match('/^#[a-fA-F0-9]{3,6}$/i', $color);
    }

    public function getList() {
        return $this->list;
    }
}

Colors::getInstance();