<?php namespace Dorigo\Colors\Test;

use PHPUnit\Framework\TestCase;
use Dorigo\Colors\Colors;

define("WP_WEBROOT", __DIR__);

class ColorsTest extends TestCase {
    /**
     * @dataProvider dataReturnsValidColor
     * @param string $color
     * @param Singleton $expectedColor
     */
    public function testReturnsValidColor($color, $expectedColor) {
        $colors = Colors::getInstance();

        $this->assertSame($colors->getColor($color), $expectedColor);
    }

    public function dataReturnsValidColor() {
        return [
            ["white", "#fff"],
            ["blue", "#006bd9"]
        ];
    }


    /**
     * @dataProvider dataReturnsValidSubColor
     * @param string $color
     * @param Singleton $expectedColor
     */
    public function testReturnsValidSubColor($color, $expectedColor) {
        $colors = Colors::getInstance();

        $color = is_string($color) ? [$color] : $color;

        $this->assertSame($colors->getColor(...$color), $expectedColor);
    }

    public function dataReturnsValidSubColor() {
        return [
            [["blue", "light"], "#17a3ff"],
            [["blue", "text"], "#fff"],
            ["light-blue", "#17a3ff"],
        ];
    }


    /**
     * @dataProvider dataNonExistantColor
     * @param string $color
     * @param Singleton $expectedException
     */

    public function testNonExistantColor($color, $expectedException) {
        $colors = Colors::getInstance();

        $color = is_string($color) ? [$color] : $color;

        $this->expectException($expectedException);
        $colors->getColor(...$color);
    }

    public function dataNonExistantColor() {
        return [
            ["NonExistantColor", \OutOfBoundsException::class],
            [["blue", "NonExistantColor"], \Exception::class],
        ];
    }

}